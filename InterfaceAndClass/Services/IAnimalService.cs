﻿namespace InterfaceAndClass.Services
{
    public interface IAnimalService
    {
        void AddAnimal(Animal animal);
    }
}
