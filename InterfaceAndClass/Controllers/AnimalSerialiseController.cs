﻿using System.Collections.Generic;

using InterfaceAndClass.Algoritms;
using InterfaceAndClass.Serializer;

using Microsoft.AspNetCore.Mvc;


namespace InterfaceAndClass.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AnimalSerialiseController : ControllerBase
    {
        private readonly ISerializer<Animal> _serializer;
        private readonly IAlgorithm<Animal> _algorithm;

        public AnimalSerialiseController(ISerializer<Animal> serializer,
            IAlgorithm<Animal> algorithm)
        {
            _serializer = serializer;
            _algorithm = algorithm;
        }

        /// <summary>
        /// Производит сериализацю данных из объекта в строку xml
        /// </summary>
        /// <returns>Возвращает xml строку</returns>
        private string GetString()
        {
            IEnumerable<Animal> animals = new List<Animal>()
            {
                new Animal { FoodType = FoodTypeEnum.Herbivore, Name = "кролик"},
                new Animal { FoodType = FoodTypeEnum.Omnivorous, Name = "тигр"},
                new Animal { FoodType = FoodTypeEnum.Predator, Name = "ёжик"}
            };

            return _serializer.Serialize(animals);
        }

        /// <summary>
        /// Получает данные из строки xml.
        /// </summary>
        /// <returns>Возвращает данные коллекции в виде строки</returns>
        [HttpGet]
        public string SerializerStart()
        {
            var xmlString = GetString();

            var animals = new XmlReader<Animal>(xmlString, _serializer, _algorithm);

            return string.Join(System.Environment.NewLine, animals);
        }

    }
}
