﻿using System;
using System.Collections.Generic;

namespace InterfaceAndClass.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Добавляет новый объект в репозиторий.
        /// </summary>
        /// <param name="item">Объект данных.</param>
        void Add(T item);

        /// <summary>
        /// Получает все данные из репозитория
        /// </summary>
        /// <returns>Возвращает коллекцию объектов.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Получает данные из файла
        /// </summary>
        /// <returns>Возвращает коллекцию объектов из файла</returns>
        IEnumerable<T> GetData();

        /// <summary>
        /// Сохраняет данные в файл.
        /// </summary>
        /// <param name="data">Коллекция объектов.</param>
        void SetData(IEnumerable<T> data);

        /// <summary>
        /// Получает объект из репозитория
        /// </summary>
        /// <param name="predicate">Условие отбора данных</param>
        /// <returns>Возвращает найденный объект.</returns>
        T GetOne(Func<T, bool> predicate);

    }
}
