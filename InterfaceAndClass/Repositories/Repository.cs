﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using InterfaceAndClass.Serializer;

namespace InterfaceAndClass.Repositories
{
    public class Repository<T> : IRepository<T>
    {
        private readonly ISerializer<T> _serializer;

        private const string FileNameTemplate = "{0}.xml";

        private readonly string _filePath = string
            .Format(FileNameTemplate, typeof(T).Name);

        public Repository(ISerializer<T> serializer)
        {
            _serializer = serializer;
        }

        public void Add(T item)
        {
            IEnumerable<T> allXml = GetData();

            allXml = allXml.Append(item).ToList();

            SetData(allXml);
        }

        public IEnumerable<T> GetAll()
        {
            var data = GetData();

            foreach (var item in data)
            {
                yield return item;
            }
        }

        public IEnumerable<T> GetData()
        {
            if (File.Exists(_filePath))
            {
                var xmlString = File.ReadAllText(_filePath);

                if (string.IsNullOrEmpty(xmlString))
                {
                    return Enumerable.Empty<T>();
                }

                // применение десериализации
                return _serializer.Deserialize(xmlString);
            }
            else
            {
                return Enumerable.Empty<T>();
            }
        }

        public void SetData(IEnumerable<T> data)
        {
            // применение сериализации
            string xmlString = _serializer.Serialize(data);

            File.WriteAllText(_filePath, xmlString);
        }

        public T GetOne(Func<T, bool> predicate)
        {
            IEnumerable<T> records = GetData();

            return records.FirstOrDefault(predicate);
        }
    }
}
