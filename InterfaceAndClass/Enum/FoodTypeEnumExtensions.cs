﻿namespace InterfaceAndClass.Enum
{
    public static class FoodTypeEnumExtensions
    {
        public static string ToFoodTypeString(this FoodTypeEnum type)
        {
            return type switch
            {
                FoodTypeEnum.Herbivore => "травоядное",
                FoodTypeEnum.Omnivorous => "хищник",
                FoodTypeEnum.Predator => "всеядное",
                _ => "Недопустимое значение.",
            };
        }
    }
}
