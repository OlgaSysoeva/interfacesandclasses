﻿using System.Collections.Generic;
using System.Xml;

using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace InterfaceAndClass.Serializer
{
    public class Serializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings XmlWriterSettings;
        private static readonly XmlReaderSettings XmlReaderSettings;

        static Serializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
            XmlReaderSettings = new XmlReaderSettings { IgnoreWhitespace = false };
        }

        public IEnumerable<T> Deserialize(string data)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();

            return serializer.Deserialize<IEnumerable<T>>(XmlReaderSettings, data);
        }

        public string Serialize(IEnumerable<T> data)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();

            return serializer.Serialize(XmlWriterSettings, data);
        }
    }
}
