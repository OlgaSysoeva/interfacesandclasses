﻿using System.Collections.Generic;

namespace InterfaceAndClass.Serializer
{
    public interface ISerializer<T>
    {
        /// <summary>
        /// Производит сериализацию коллекции в строку xml
        /// </summary>
        /// <param name="data">Коллекция объектов.</param>
        /// <returns>Возвращает строку xml.</returns>
        string Serialize(IEnumerable<T> data);

        /// <summary>
        /// Производит десериализацию строки xml в коллекцию объектов
        /// </summary>
        /// <param name="data">Строка xml</param>
        /// <returns>Возвращает коллекцию объектов.</returns>
        IEnumerable<T> Deserialize(string data);
    }
}
