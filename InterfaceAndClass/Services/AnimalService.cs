﻿using System;
using System.Collections.Generic;
using System.Linq;

using InterfaceAndClass.Repositories;

namespace InterfaceAndClass.Services
{
    public class AnimalService : IAnimalService
    {
        private readonly IRepository<Animal> _repository;

        public AnimalService(IRepository<Animal> repository)
        {
            _repository = repository;
        }

        public void AddAnimal(Animal animal)
        {
            // валидация
            if (animal == null)
                throw new ArgumentNullException(nameof(animal));

            if (string.IsNullOrEmpty(animal.Name))
                throw new ArgumentException(nameof(animal));

            // поиск животного в файле
            var animalFind = GetOneAnimal(animal.Name);

            if (animalFind != null)
            {
                throw new ArgumentException("An element already exists.");
            }

            _repository.Add(animal);
        }
        
        public Animal GetOneAnimal(string name)
        {
            return _repository.GetOne(x => x.Name == name);
        }
    }
}
