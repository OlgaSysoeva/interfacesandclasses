﻿using System;
using System.Collections.Generic;

using System.Linq;

namespace InterfaceAndClass.Algoritms
{
    public class Algorithm<T> : IAlgorithm<T> where T: IComparable<T>
    {
        public IEnumerable<T> Sorting(IEnumerable<T> data)
        {
            return data.OrderBy(x => x);
        }
    }
}
