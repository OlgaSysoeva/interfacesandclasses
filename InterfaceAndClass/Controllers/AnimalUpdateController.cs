﻿using InterfaceAndClass.Services;

using Microsoft.AspNetCore.Mvc;
using System;

namespace InterfaceAndClass.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AnimalUpdateController : ControllerBase
    {
        private readonly IAnimalService _animalService;

        public AnimalUpdateController(IAnimalService animalService)
        {
            _animalService = animalService;
        }

        /// <summary>
        /// Добавляет объект класса Animal.
        /// </summary>
        /// <param name="animal">Данные объекта</param>
        /// <returns>Возвращает результат в виде строки.</returns>
        [HttpPost]
        public IActionResult AddAnimal(Animal animal)
        {
            if (animal == null)
            {
                return BadRequest();
            }

            try
            {
                _animalService.AddAnimal(animal);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}