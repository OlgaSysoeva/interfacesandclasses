﻿namespace InterfaceAndClass
{
    public enum FoodTypeEnum
    {
        /// <summary>
        /// травоядное
        /// </summary>
        Herbivore,

        /// <summary>
        /// хищник
        /// </summary>
        Predator,

        /// <summary>
        /// всеядное
        /// </summary>
        Omnivorous
    }
}
