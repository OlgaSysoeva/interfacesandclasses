﻿using System.Collections.Generic;

namespace InterfaceAndClass.Algoritms
{
    /// <summary>
    /// Предоставляет механизмы обработки данных.
    /// </summary>
    /// <typeparam name="T">Тип объекта коллекции.</typeparam>
    public interface IAlgorithm<T>
    {
        /// <summary>
        /// Сортирует даннные коллекции.
        /// </summary>
        /// <param name="data">Коллекция данных.</param>
        /// <returns>Возвращает отсортированную коллекцию данных.</returns>
        IEnumerable<T> Sorting(IEnumerable<T> data);
    }
}
