﻿using InterfaceAndClass.Enum;
using System;
using System.Diagnostics.CodeAnalysis;

namespace InterfaceAndClass
{
    public class Animal : IComparable<Animal>
    {
        /// <summary>
        /// Тип питания животного.
        /// </summary>
        public FoodTypeEnum FoodType { get; set; }

        /// <summary>
        /// Наименование животного.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Сравнивает объекты.
        /// </summary>
        /// <param name="other">Объект для сравнения.</param>
        /// <returns>Возвращает результат сравнения в виде числа.</returns>
        public int CompareTo([AllowNull] Animal other)
        {
            var foodTypeThis = this.FoodType.ToFoodTypeString();
            var foodTypeOther = other?.FoodType.ToFoodTypeString();
            
            return foodTypeThis.CompareTo(foodTypeOther);
        }

        /// <summary>
        /// Приводит объект к строковому представлению.
        /// </summary>
        /// <returns>Возвращает строку с данными объекта.</returns>
        public override string ToString()
        {
            return $"Тип питания: {FoodType.ToFoodTypeString()}, Название животного: {Name}";
        }
    }
}
