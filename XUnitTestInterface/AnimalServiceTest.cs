using System;
using System.Collections.Generic;
using System.Linq;

using Xunit;

using Moq;

using InterfaceAndClass.Services;
using InterfaceAndClass;
using InterfaceAndClass.Repositories;

namespace XUnitTestInterface
{
    public class AnimalServiceTest
    {
        [Fact]
        public void AddAnimalTest()
        {

            var animalList =  new List<Animal>()
            {
                new Animal { FoodType = FoodTypeEnum.Herbivore, Name = "кролик"},
                new Animal { FoodType = FoodTypeEnum.Omnivorous, Name = "тигр"},
                new Animal { FoodType = FoodTypeEnum.Predator, Name = "ёжик"}
            };

            var animalTest = new Animal { FoodType = FoodTypeEnum.Herbivore, Name = "белка" };

            var mock = new Mock<IRepository<Animal>>();
            mock.Setup(rep => rep.GetAll());
            mock.Setup(rep => rep.GetOne(It.IsAny<Func<Animal, bool>>()));
            mock.Setup(rep => rep.Add(It.IsAny<Animal>()));
            mock.Setup(rep => rep.GetData())
                .Returns(() =>
                {
                    return animalList;
                });
            mock.Setup(rep => rep.SetData(It.IsAny<IEnumerable<Animal>>()))
                .Callback((IEnumerable<Animal> data) =>
                {
                    animalList = data.ToList();
                });

            var animalService = new AnimalService(mock.Object);

            animalService.AddAnimal(animalTest);

            mock.Verify();

            Assert.Contains(animalTest, animalList);

        }

    }
}
