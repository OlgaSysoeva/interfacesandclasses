﻿using System;
using System.Collections;
using System.Collections.Generic;

using InterfaceAndClass.Algoritms;
using InterfaceAndClass.Serializer;

namespace InterfaceAndClass
{
    public class XmlReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly ISerializer<T> _serializer;
        private readonly IAlgorithm<T> _algorithm;
        private readonly string _file;

        public XmlReader(string file, 
            ISerializer<T> serializer,
            IAlgorithm<T> algorithm)
        {
            _serializer = serializer;
            _file = file;
            _algorithm = algorithm;
        }

        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {   
                }
            }

            this.disposedValue = true;
        }

        ~XmlReader()
        {
            Dispose(false);
        }

        private IEnumerable<T> _items;
        public IEnumerator<T> GetEnumerator()
        {
            _items = _serializer.Deserialize(_file);

            var sortItem = _algorithm.Sorting(_items);

            foreach (var item in sortItem)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
